<?php
require_once 'classes/DB.php';
header("Content-type: application/json");           // Send back json data

$fname = $_SERVER['HTTP_X_ORIGINALFILENAME'];       // Get extra parameters
$fsize = $_SERVER['HTTP_X_ORIGINALFILESIZE'];
$mimetype = $_SERVER['HTTP_X_ORIGINALMIMETYPE'];
$aircraftId = $_SERVER['HTTP_X_AIRCRAFTID'];

$status = $db->createCraftImage($aircraftId, $mimetype, $fname);
if ($status['status']=='OK') {
  $id = $status['id'];
} else {
  echo json_encode(array('status'=>'FAIL', 'msg'=>'Unable to insert into DB'));
}

$handle = fopen('php://input', 'r');                // Read the file from stdin
$output = fopen('images/'.$id, 'w');
$contents = '';

while (!feof($handle)) {                            // Read in blocks of 8 KB (no file size limit)
    $contents = fread($handle, 8192);
    fwrite($output, $contents);
}
fclose($handle);
fclose($output);

$imgData = file_get_contents('images/'.$id);

$img = imagecreatefromstring($imgData);
$thumb = scale ($img, 200, 200);

$db->updateCraftThumbnail($id, $thumb);
if ($status['status']=='OK') {
  $id = $status['id'];
} else {
  echo json_encode(array('status'=>'FAIL', 'msg'=>'Unable to update DB with thumbnail image.'));
}

$data = array ('status'=>'OK', 'id'=>$id, 'fname'=>$fname, 'size'=>$fsize, 'mime'=>$mimetype);

echo json_encode($data);

// Fra forelesningen uke5, image_upload_and_scale
function scale ($img, $new_width, $new_height) {
  $old_x = imageSX($img);
  $old_y = imageSY($img);

  if($old_x > $old_y) {                     // Image is landscape mode
    $thumb_w = $new_width;
    $thumb_h = $old_y*($new_height/$old_x);
  } else if($old_x < $old_y) {              // Image is portrait mode
    $thumb_w = $old_x*($new_width/$old_y);
    $thumb_h = $new_height;
  } if($old_x == $old_y) {                  // Image is square
    $thumb_w = $new_width;
    $thumb_h = $new_height;
  }

  if ($thumb_w>$old_x) {                    // Don't scale images up
    $thumb_w = $old_x;
    $thumb_h = $old_y;
  }

  $dst_img = ImageCreateTrueColor($thumb_w,$thumb_h);
  imagecopyresampled($dst_img,$img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y);

  ob_start();                         // flush/start buffer
  imagepng($dst_img,NULL,9);          // Write image to buffer
  $scaledImage = ob_get_contents();   // Get contents of buffer
  ob_end_clean();                     // Clear buffer
  return $scaledImage;
}
