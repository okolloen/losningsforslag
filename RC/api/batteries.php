<?php

require_once '../classes/DB.php';

header ('Content-Type: application/json');

// Return information about batteries as JSON data
echo json_encode ($db->getBatteries());
