<?php
require_once '../classes/DB.php';

$status = $db->registerBatteryStatus($_POST);
if ($status['status']=='OK') {
  $msg = 'Ny batteristatus er registrert.';
} else {
  $msg = 'Kunne ikke legge til ny batteristatus : '.$status['error'][2];
}

// Return as JSON data
echo json_encode(array('status'=>$status, 'msg'=>$msg));
