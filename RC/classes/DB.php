<?php

/**
 * All database interactions happens through an object of this class.
 *
 * The constructor connects to the database and then there are methods
 * that adds to and gets data from the different database tables.
 */
class DB {
  private $db = null;
  private $error = null;

  /**
   * Connects to the database, set the variable error to the exception thrown
   * if unable to connect to the database.
   */
  function __construct() {
    try {
      $this->db = new PDO ('mysql:dbname=imt2291_eksamen2018;host=127.0.0.1', 'root', '');
    } catch (Exception $error) {
      $this->error = $error;
    }
  }

  /**
   * Return the actual database (PDO) object.
   * @return PDO the pdo object that contains the connection used by this object.
   */
  function getDB() {
    return $this->db;
  }

  /**
   * Get the exception (if any) that was thrown when attempting to connect
   * to the database.
   *
   * @return Exception the exception thrown when trying to connect to the
   * database, or null if no exception was thrown.
   */
  function getError() {
    return $this->error;
  }

  /**
   * Method used to add a new battery to the database. To be called with an
   * assosiative array with information about the battery to be added.
   *
   * @param Array $batteryData assosiative array with elements : id, cells,
   * capacity, cRating and purchaseDate.
   * @return Array an assosiative array with elements status set to 'OK' on
   * success or 'FAIL' if the operation failed. If status=>'FAIL' then the
   * element 'error' contains information about what went wrong.
   */
  function addBattery($batteryData) {
    $sql = 'INSERT INTO batteries (id, cells, capacity, maxDischarge, purchaseDate) VALUES (?, ?, ?, ?, ?)';
    $stmnt = $this->db->prepare($sql);
    $stmnt->execute (array($batteryData['id'], $batteryData['cells'], $batteryData['capacity'], $batteryData['cRating'], $batteryData['purchaseDate']));
    if ($stmnt->rowCount()==1) {
      return array('status'=>'OK');
    } else {
      return array('status'=>'FAIL', 'error'=>$stmnt->errorInfo());
    }
  }

  /**
   * Method used to add a new aircraft to the database. To be called with an
   * assosiative array with information about the aircraft to be added.
   *
   * @param Array $aircraftData assosiative array with elements : name, fpv,
   * camera.
   * @return Array an assosiative array with elements status set to 'OK' on
   * success or 'FAIL' if the operation failed. If status=>'FAIL' then the
   * element 'error' contains information about what went wrong.
   */
  function addAircraft($aircraftData) {
    $sql = 'INSERT INTO aircrafts (name, fpv, camera) VALUES (?, ?, ?)';
    $stmnt = $this->db->prepare($sql);
    $stmnt->execute (array($aircraftData['name'], isset($aircraftData['fpv']), isset($aircraftData['camera'])));
    if ($stmnt->rowCount()==1) {
      return array('status'=>'OK');
    } else {
      return array('status'=>'FAIL', 'error'=>$stmnt->errorInfo());
    }
  }

  /**
   * Method used to register battery status in the database. To be called with an
   * assosiative array with information about the battery status to be added.
   *
   * @param Array $batteryStatus assosiative array with elements : craftId, batteryId,
   * flightTime, capacityRemaining, flightDate.
   * @return Array an assosiative array with elements status set to 'OK' on
   * success or 'FAIL' if the operation failed. If status=>'FAIL' then the
   * element 'error' contains information about what went wrong.
   */
  function registerBatteryStatus($batteryStatus) {
    $sql = 'INSERT INTO batteryStatus (craftId, batteryId, flightTime, capacityRemaining, flightDate) VALUES (?, ?, ?, ?, ?)';
    $stmnt = $this->db->prepare($sql);
    $stmnt->execute (array($batteryStatus['aircraft'], $batteryStatus['batterypack'],
                          $batteryStatus['flightTime'], $batteryStatus['remaining'],
                          $batteryStatus['flightDate']));
    if ($stmnt->rowCount()==1) {
      return array('status'=>'OK');
    } else {
      return array('status'=>'FAIL', 'error'=>$stmnt->errorInfo());
    }
  }

  /**
   * Creates a new entry in the aircraftImages table for a new image for a given aircraft.
   *
   * @param  integer $aircraftid the id of the aircraft that this is an image of
   * @param  String $mimetype   the mimetype of this images
   * @param  String $fname      the original filename for this image
   * @return Array              an assosiative array, on success it contains
   * status=>'OK' and 'id'=>the id for the newly created database record.
   * on failure status=>'FAIL' and error=>is a message explaining what went wrong.
   */
  function createCraftImage($aircraftid, $mimetype, $fname) {
    $sql = 'INSERT INTO aircraftImages (mimeType, filename, craftId, dateAdded) VALUES (?, ?, ?, now())';
    $stmnt = $this->db->prepare($sql);
    $stmnt->execute (array($mimetype, $fname, $aircraftid));
    if ($stmnt->rowCount()==1) {
      return array('status'=>'OK', 'id'=>$this->db->lastInsertId());
    } else {
      return array('status'=>'FAIL', 'error'=>$stmnt->errorInfo());
    }
  }

  /**
   * Method used to update the thumbnail image of a record in the aircraftImages
   * table.
   *
   * @param  integer $id        the id of the record to update
   * @param  String $thumbnail  a string containing the new thumbnail image
   * @return Array an assosiative array with elements status set to 'OK' on
   * success or 'FAIL' if the operation failed. If status=>'FAIL' then the
   * element 'error' contains information about what went wrong.
   */
  function updateCraftThumbnail($id, $thumbnail) {
    $sql = 'UPDATE aircraftImages SET media=? WHERE id=?';
    $stmnt = $this->db->prepare($sql);
    $stmnt->execute (array($thumbnail, $id));
    if ($stmnt->rowCount()==1) {
      return array('status'=>'OK');
    } else {
      return array('status'=>'FAIL', 'error'=>$stmnt->errorInfo());
    }
  }

  /**
   * Method used to get information about the aircrafts that is registered in
   * the database.
   *
   * @return Array an array with one item for each aircraft, each item is
   * an assosiative array with elements : id, name, fpv, camera
   */
  function getAircrafts() {
    $sql = 'SELECT id, name, fpv, camera FROM aircrafts ORDER BY name';
    $stmnt = $this->db->prepare($sql);
    $stmnt->execute (array());
    return $stmnt->fetchAll(PDO::FETCH_ASSOC);
  }

  /**
   * Method used to get information about the batteries that is registered in
   * the database.
   *
   * @return Array an array with one item for each battery, each item is
   * an assosiative array with elements : id, cells, capacity, maxDischarge, purchaseDate.
   */
  function getBatteries() {
    $sql = 'SELECT id, cells, capacity, maxDischarge, purchaseDate FROM batteries ORDER BY id';
    $stmnt = $this->db->prepare($sql);
    $stmnt->execute (array());
    return $stmnt->fetchAll(PDO::FETCH_ASSOC);
  }

  /**
   * Method used to get thumbnail from database.
   *
   * @param  integer $id the id of the thumbnail to return
   * @return String     the thumbnail for the given id, or empty string if no such thumbnail
   */
  function getThumbnail($id) {
    $sql = 'SELECT media FROM aircraftImages WHERE id=?';
    $stmnt = $this->db->prepare($sql);
    $stmnt->execute (array($id));
    if ($res = $stmnt->fetch()) {
      return $res['media'];
    }
    return '';
  }
}

$db = new DB();
