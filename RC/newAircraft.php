<?php
// Twig installed with : composer require "twig/twig:^2.0"
// To reinstall when cloned from bitbucket, run : composer install
// in the folder where this file is located
require_once 'vendor/autoload.php';   // To use twig

$msg = null;
if (isset($_POST['name'])) {          // User has filled in the form and want to store a new aircraft
  require_once 'classes/DB.php';
  $status = $db->addAircraft($_POST);
  if ($status['status']=='OK') {      // Aircraft successfully added
    $msg = 'Nytt fartøy er lagt til.';
  } else {                            // Something went wrong when adding the aircraft
    $msg = 'Kunne ikke legge til nytt fartøy : '.$status['error'][2];
  }
}

// Set up twig
$loader = new Twig_Loader_Filesystem('twig');
$twig = new Twig_Environment($loader, array());

// Show the page, with aircraft added successfully/failed message if appropriate.
echo $twig->render('newAircraft.html', array('msg'=>$msg));
