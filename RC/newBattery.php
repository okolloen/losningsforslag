<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>RC - add battery</title>
  <style media="screen">
    label {
      display: inline-block;
      width: 7em;
    }

    input[type="number"] {
      width: 5em;
    }
  </style>
</head>
<?php
if (isset($_POST['id'])) {  // User has entered data into the form and submitted
  require_once 'classes/DB.php';
  $status = $db->addBattery($_POST);  // Functionality is in the DB class
  if ($status['status']=='OK') {      // Battery added successfully
    $msg = 'Nytt batteri er lagt til.';
  } else {                            // Battery could not be added
    $msg = 'Kunne ikke legge til nytt batteri : '.$status['error'][2];
  }
}
 ?>
<body>
  <h1>Registrer nytt batteri</h1>
  <?php
    if (isset($msg)) {                // Battery added fail/success
      echo "<h2>$msg</h2>";
    }
  ?>
  <form method="post" action="newBattery.php">
    <label for="id">Id:</label><input type="number" id="id" name="id" min="1" max="1000"><br>
    <label for="cells">Celler</label><input type="number" id="cells" name="cells" value="3" min="1" max="24"><br>
    <label for="capacity">Kapasitet (mAh)</label><input type="number" id="capacity" name="capacity" value="1500" min="50" max="20000"><br>
    <label for="cRating">C-rating</label><input type="number" id="cRating" name="cRating" value="30" min="1" max="200"><br>
    <label for="purchaseDate">Purchase date</label><input type="date" id="purchaseDate" name="purchaseDate"><br>
    <input type="submit" name="submit" value="Lagre informasjon">
  </form>
</body>
</html>
