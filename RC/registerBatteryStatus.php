<?php
require_once 'vendor/autoload.php';   // For twig
require_once 'classes/DB.php';        // Always need DB connection, to get batteries and aircrafts

$aircrafts = $db->getAircrafts();
$batteries = $db->getBatteries();

$msg = null;
if (isset($_POST['flightTime'])) {    // User has filled in the form and want to add a batteryStatus
  $status = $db->registerBatteryStatus($_POST);
  if ($status['status']=='OK') {      // Battery status successfully added to database
    $msg = 'Ny batteristatus er registrert.';
  } else {                            // Unable to add battery status to database
    $msg = 'Kunne ikke legge til ny batteristatus : '.$status['error'][2];
  }
}

// Set up Twig
$loader = new Twig_Loader_Filesystem('twig');
$twig = new Twig_Environment($loader, array());

$now = date('Y-m-d');   // For autofilling in todays date as flight date
echo $twig->render('registerBatteryStatus.html', array('msg'=>$msg, 'aircrafts'=>$aircrafts, 'batteries'=>$batteries, 'now'=>$now));
