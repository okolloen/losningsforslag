<?php
require_once 'vendor/autoload.php';   // For twig
require_once 'classes/DB.php';        // Needed to get aircrafts

$aircrafts = $db->getAircrafts();

// Set up Twig
$loader = new Twig_Loader_Filesystem('twig');
$twig = new Twig_Environment($loader, array());

echo $twig->render('uploadAircraftImage.html', array('aircrafts'=>$aircrafts));
