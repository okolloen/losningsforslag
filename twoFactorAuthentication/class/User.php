<?php

/**
 * There will be one object of this class representing the state
 * of the user.
 *
 * Creating this object the state of the user will be determined
 * by the SESSION variables or POST/GET variables.
 *
 * @author �ivind Kolloen
 * @NOTE this code was given to the student on request, see Attempt_User.php for the students code.
 */
class User {
  private $userid = -1;
  private $loginVerify = false;
  private $msg = '';

  /**
   * Uses the SESSION['loginInProgress'], SESSION['uid'],
   * POST['uname'], POST['loginCode'], GET['logout']
   * to determine the state of the user.
   */
  function __construct() {
    if (isset($_SESSION['loginInProgress'])) {
      $this->loginVerify = true;
    }
    if (isset($_POST['uname'])) {
      $this->loginStep1();
    } else if (isset($_POST['loginCode'])) {
      $this->loginStep2();
    } else if (isset($_GET['logout'])) {
      $this->logout();
    } else if (isset($_SESSION['uid'])) {
      $this->useSession();
    }
  }

  /**
   * returns  string containing the correct form for loging in,
   * entering verification code or loging out depending on the
   * user state.
   * @return String with the html for the correct form
   */
  function loginForm() {
    if ($this->userid>=0) { // User logged in, show logout
      return '<a href="index.php?logout=true">Log out</a>';
    } else if ($this->loginVerify) { // username/pwd correct, ask for verification code
      return $this->msg.'<form method="POST" action="index.php"><label for="confirm">Verification code</label><input type="password" name="loginCode" id="confirm"><br><input type="submit" value="Confirm code"><br>(Pssst, the code is: <span id="code">'.$_SESSION['verificationCode'].'</span>)</form>';
    } else { // Not logged in, show uname/pwd form.
      return $this->msg.'<form method="POST" action="index.php"><label for="uname">Username</label><input type="text" name="uname" id="uname"><br><label for="pwd">Password</label><input type="password" name="pwd" id="pwd"><br><input type="submit" value="Log in"></form>';
    }
  }

  /**
   * POST['uname'] and POST['pwd'] detected, look up the user in
   * the database and create the verification code. Store this in
   * SESSION[verificationCode] and set SESSION['loginInProgress'].
   *
   * @return String the verification code generated
   */
  function loginStep1() {
    if ($_POST['uname']=='user'&&$_POST['pwd']=='pwd') {
      $verificationCode = substr(md5(date('l jS \of F Y h:i:s A')), 0,4);
      $_SESSION['verificationCode'] = $verificationCode;
      $_SESSION['loginInProgress'] = 'true';
      $_SESSION['tuid'] = 1;
      $this->loginVerify = true;
      return $verificationCode;
    } else {
      $this->msg = 'Bad username/pwd<br>';
    }
  }

  /**
   * POST['loginCode'] detected, check this against the
   * SESSION['verificationCode'] and if correct set SESSION['uid']
   * to be equal to the UID of the logged in user. The user is
   * now logged in.
   * If a missmatch set the msg to reflect this in the verification
   * code entry form.
   * @return integer the UID of the logged in user
   */
  function loginStep2() {
    if ($_POST['loginCode']==$_SESSION['verificationCode']) {
      unset($_SESSION['verificationCode']);
      unset($_SESSION['loginInProgress']);
      $_SESSION['uid'] = $_SESSION['tuid'];
      $this->userid = 1;
      return 1;
    } else {
      $this->msg = 'Bad verification code<br>';
    }
  }

  /**
   * GET['logout'] is detected, remove the SESSION['uid'] variable.
   * The user is now logged out.
   */
  function logout() {
    unset ($_SESSION['uid']);
  }

  /**
   * SESSION['uid'] is detected, this means the user is logged in.
   * Set the userid in the class to SESSION['uid'].
   */
  function useSession() {
    $this->userid = $_SESSION['uid'];
  }

  /**
   * Check login status of user, returns the userid of the logged
   * in user (IE, -1 if not logged in).
   *
   * @return integer the userid of the logged in user or -1 if not logged in
   */
  function loggedIn() {
    return $this->userid;
  }
}

// Always create an instance of the User class when this file is
// included.
$user = new User();
