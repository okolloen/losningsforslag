<?php
// Test framework installed via : composer require --dev phpunit/phpunit ^6
// To install when downloaded from bitbucket use : composer install

require_once 'vendor/autoload.php';
require_once 'class/User.php';

use PHPUnit\Framework\TestCase;

final class UserTest extends TestCase {
  /**
   * When creating a new User object the user should not be logged in.
   * The loggedIn method should return -1
   */
  public function testDefaultNotLoggedIn() {
    $user = new User();
    $this->assertEquals(
            -1,
            $user->loggedIn()
        );
  }

  /**
   * If the session variable 'uid' is set when creating a new User object
   * the user with that uid should be considered logged in.
   * The loggedIn method should return the value given in the session
   * variable 'uid'.
   */
  public function testLoginThroughSession() {
    $_SESSION['uid'] = 1;
    $user = new User();
    $this->assertEquals(
            1,
            $user->loggedIn()
        );
  }

  /**
   * If called with $_POST['uname']=='user' and $_POST['pwd']='pwd' the
   * method loginStep1 should return a string with four characters.
   */
  public function testLoginStep1() {
    $_POST['uname'] = 'user';
    $_POST['pwd'] = 'pwd';
    $user = new User();
    $code = $user->loginStep1();
    $this->assertEquals(
            4,
            strlen($code)
        );
  }

  /**
   * Use loginStep1 with correct username and password to set SESSION values and
   * generate the secret code. Then when loginStep2 is called with this secret
   * code set as $_POST['loginCode'] we should be logged in as user with userid=1
   */
  public function testLoginStep2() {
    $_POST['uname'] = 'user';
    $_POST['pwd'] = 'pwd';
    $user = new User();
    $code = $user->loginStep1();
    $user = new User();
    unset($_POST['uname']);
    unset($_POST['pwd']);
    $_POST['loginCode'] = $code;
    $this->assertEquals(
            1,
            $user->loginStep2()
        );
  }

  /**
   * First create a new User object with $_POST['uname'] and $_POST['pwd'] set.
   * Then unset the two $_POST variables and set $_POST['loginCode'] to the
   * value of $_SESSION['verificationCode'] and create a new Object of User.
   * The value of $_SESSION['uid'] should now be "1".
   * @return [type] [description]
   */
  public function testLogin() {
    unset($_SESSION['uid']);
    $_POST['uname'] = 'user';
    $_POST['pwd'] = 'pwd';
    $user = new User();
    unset($_POST['uname']);
    unset($_POST['pwd']);
    $_POST['loginCode'] = $_SESSION['verificationCode'];
    $user = new User();
    $this->assertEquals(
            1,
            $_SESSION['uid']
        );
  }

  /**
   * If we are logged in and set $_GET['logout'] to true and create a new
   * User object we should then be logged out.
   */
  public function testLogout() {
    $this->testLogin();
    unset($_POST['loginCode']);
    $_GET['logout'] = 'true';
    $user = new User();
    $this->assertEquals(
            -1,
            $user->loggedIn()
        );
  }
}
