<?php
// Test rammeverk installert via : composer require --dev  behat/mink behat/mink-goutte-driver
// For å installere etter å ha lastet ned fra bitbucket bruk : composer require --dev  behat/mink behat/mink-goutte-driver

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use Behat\Mink\Element\DocumentElement;
use Behat\Mink\Element\NodeElement;

/**
 * Class contains all functional tests for this web application.
 *
 */
class FunctionalTests extends TestCase {
  /* Change this to suite your server setup */
  protected $baseUrl = "http://localhost:8080/index.php";
  protected $session;
  protected $pageTitles = array ('mainPage' => 'Login test');

  /**
   * Initiates the testing session
   */
  protected function setup() {
    $driver = new \Behat\Mink\Driver\GoutteDriver();
    $this->session = new \Behat\Mink\Session($driver);
    $this->session->start();
  }

  /**
   * Gets the head/title value for the given page.
   */
  private function getPageTitle($page) {
    $title = null;
    $titleEl = $page->find('xpath', 'head/title');

    // Page has a title element
    if ($titleEl) {
        $title = $titleEl->getText();
    }
    return $title;
  }

  /**
   * Simple test to see if we can get the title of the page.
   */
  public function testCanGetPage() {
    $this->session->visit($this->baseUrl);
    $page = $this->session->getPage();

    $this->assertEquals ($this->pageTitles['mainPage'],
                         $this->getPageTitle($page), 'Wrong title on page');
  }

  /**
   * When in a new session, we should not be logged in
   */
  public function testNotInitiallyLoggedIn() {
    $this->session->visit($this->baseUrl);
    $page = $this->session->getPage();

    $form = $page->find('xpath', 'body/form');  // First form under body

    if ($form!=null) {
      $input = $form->find('css', 'input[id="uname"]');
      if ($input==null) {
        $this->assertTrue(false, 'Input field for username not found');
      } else {
        $this->assertTrue(true, 'Everything ok');
      }
    } else {
      $this->assertTrue(false, 'Login form not found');
    }
  }

  /**
   * First enter username and pwd, then the verification code and then we should
   * be logged in
   */
  public function testTwoStageLogin() {
    $this->session->visit($this->baseUrl);
    $page = $this->session->getPage();

    $form = $page->find('xpath', 'body/form');  // First form under body

    if ($form!=null) {
      $input = $form->find('css', 'input[id="uname"]');
      if ($input!=null) {
        $input->setValue('user');
      } else {
        $this->assertTrue(false, 'Input field for username not found');
      }
      $input = $form->find('css', 'input[id="pwd"]');
      if ($input!=null) {
        $input->setValue('pwd');
      } else {
        $this->assertTrue(false, 'Input field for password not found');
      }
    } else {
      $this->assertTrue(false, 'Login form not found');
    }
    $form->submit();        // Send username and password

    // We should now be on stage two
    $page = $this->session->getPage();

    $input = $page->find('css', 'input[id="confirm"]');
    if ($input!=null) {
      $code = $form->find('css', 'span[id="code"]')->getText();
      $input->setValue($code);
    } else {
      $this->assertTrue(false, 'Input field for verification code not found');
    }
    $form->submit();    // Send verification code

    // We should now be logged in
    $page = $this->session->getPage();

    $logout = $page->find('css', 'a');
    if ($logout!=null) {
      $this->assertEquals('Log out',$logout->getText(), 'Wrong text in log out link');
    } else {
      $this->assertTrue(false, 'No log out link found');
    }
  }

  /**
   * Test that we can actually log out
   */
  public function testLogOut() {
    $this->testTwoStageLogin();   // First log in
    $page = $this->session->getPage();
    $logout = $page->find('css', 'a');
    if ($logout!=null) {
      $logout->click();           // Click the link to log out
    } else {
      $this->assertTrue(false, 'No log out link found');
    }

    // We should now be logged out
    $page = $this->session->getPage();
    $form = $page->find('xpath', 'body/form');  // First form under body

    if ($form!=null) {
      $input = $form->find('css', 'input[id="uname"]');
      if ($input!=null) {
        $input->setValue('user');
      } else {
        $this->assertTrue(false, 'Input field for username not found');
      }
    } else {
      $this->assertTrue(false, 'Not logged out');
    }
  }
}
